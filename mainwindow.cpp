#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "appconfig.h"
#include "settingsdialog.h"

#include <QDir>
#include <QFileDialog>
#include <QTextStream>
#include <QDebug>
#include <QMessageBox>
#include <QTableWidget>
#include <QProcess>
#include <QStack>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_but_cfgReload_clicked()
{
    QFile *file = AppConfig::getOpenmwCfgFile();
    QTableWidget *tbl = ui->tbl_cfgLines;

    tbl->setRowCount(0);

    if (!file->open(QIODevice::ReadOnly | QIODevice::Text)) {
        return;
    }

    QTextStream stream(file);
    while (!stream.atEnd()) {
        QString line = stream.readLine();
        int newRowIndex = tbl->rowCount();

        QStringList linesplit = line.split("=");
        tbl->insertRow(newRowIndex);

        tbl->setItem(newRowIndex, 0, new QTableWidgetItem(linesplit.takeFirst()));
        tbl->setItem(newRowIndex, 1, new QTableWidgetItem(linesplit.join("=")));
    }

    file->close();
}

void MainWindow::on_but_cfgSave_clicked()
{
    QString filecontents = "";
    QTableWidget *tbl = ui->tbl_cfgLines;
    for(int i=0; i < tbl->rowCount(); i++) {
        QString prop = tbl->item(i, 0)->text();
        QString value = tbl->item(i, 1)->text();
        filecontents += prop + "=" + value;

        if (i < tbl->rowCount()) {
            filecontents += "\n";
        }
    }

    QFile newfile;
    newfile.setFileName(AppConfig::getOpenmwCfgFilePath());
    if (!newfile.open(QIODevice::ReadWrite))
        return;

    newfile.resize(0);

    QTextStream out(&newfile);
    out << filecontents;
    newfile.flush();
    newfile.close();
}

void MainWindow::on_but_modArchRefresh_clicked()
{
    QTableWidget *tbl = ui->tbl_modArch;
    QDir *dir = AppConfig::getArchFolder();

    QStringList archiveTypes;
    archiveTypes << "*.zip" << "*.rar" << "*.7z" << "*.7zip" << "*.tar.gz";
    dir->setNameFilters(archiveTypes);

    QStringList filenames = dir->entryList();
    tbl->setEditTriggers(QAbstractItemView::NoEditTriggers);
    tbl->setRowCount(0);

    for (int i=0; i<filenames.length(); i++) {
        tbl->insertRow(i);
        tbl->setItem(i,0, new QTableWidgetItem(filenames.at(i)));
    }
}

void MainWindow::on_tbl_modArch_itemSelectionChanged()
{
    ui->but_modExtract->setEnabled(true);
}

void MainWindow::on_tbl_modInstalled_itemSelectionChanged()
{
    ui->but_modDelete->setEnabled(true);
}

void MainWindow::on_but_settings_clicked()
{
    SettingsDialog sw(this);
    sw.exec();
}

void MainWindow::on_but_modExtract_clicked()
{
    QDir *modFolder = AppConfig::getModFolder();

    for (QTableWidgetItem *item : ui->tbl_modArch->selectedItems()) {
        QString archFileName = item->text();
        QString newFolderName = archFileName.left(archFileName.lastIndexOf('.'));
        QString fullArchPath = AppConfig::getArchFolderPath() + "/" + archFileName;
        QString fullInstPath = modFolder->absolutePath() + "/" + newFolderName;

        if (!QDir(fullInstPath).exists() && !modFolder->mkdir(newFolderName)) {
            QMessageBox msgbox;
            msgbox.setText("Unable to create folder for this mod.\nDo you have write permissions?");
            msgbox.setIcon(QMessageBox::Critical);
            msgbox.exec();
            break;
        }

        extractArchive(fullArchPath, fullInstPath);

        // Add entry to installed mods
        QTableWidget *tbl_installed = ui->tbl_modInstalled;
        if (tbl_installed->findItems(newFolderName, Qt::MatchFixedString).size() == 0) {
            int newRowIndex = tbl_installed->rowCount();
            tbl_installed->insertRow(newRowIndex);
            tbl_installed->setItem(newRowIndex,0, new QTableWidgetItem(newFolderName));
        }
    }
}

void MainWindow::extractArchive(QString archivePath, QString targetParentFolder) {
    QString os = QSysInfo::kernelType();
    if (os == "linux") {
        QString unar = AppConfig::getUnarExecPath();
        QProcess *prog = new QProcess(this);

//        prog->

//        prog->start(path, arguments);

//        prog->setProcessChannelMode(QProcess::MergedChannels);
//        QString program = AppConfig::getUnarExecPath();
//        QStringList arguments;
//        qWarning() << program + " -f -q -o " + '"' + targetParentFolder + '" "' + archivePath + '"';
    }
    addModToCfg(targetParentFolder);
}

void MainWindow::addModToCfg(QString path) {

    QFile *file = AppConfig::getOpenmwCfgFile();

    if (!file->open(QIODevice::ReadOnly | QIODevice::Text)) {
        return;
    }

    QStringList items = QTextStream(file).readAll().split('\n');
    file->close();
    int lastDataIndex = 0;
    for (int n=0; n < items.length(); n++) {
        if (items.at(n).startsWith("data")) {
            lastDataIndex = n;
        }
    }

    items.insert(lastDataIndex+1, "data=\"" + path + "\"");

    QTableWidget *tbl = ui->tbl_cfgLines;
    tbl->setRowCount(0);
    for (int n=0; n < items.length(); n++) {
        if (items.at(n).length() > 0) {
            QStringList linesplit = items.at(n).split("=");
            tbl->insertRow(n);
            tbl->setItem(n, 0, new QTableWidgetItem(linesplit.takeFirst()));
            tbl->setItem(n, 1, new QTableWidgetItem(linesplit.join("=")));
        }
    }

    this->on_but_cfgSave_clicked();
}

void MainWindow::refreshAllTabs() {
    on_but_modArchRefresh_clicked();
    on_but_modInstRefresh_clicked();
    on_but_cfgReload_clicked();
}

void MainWindow::on_but_modInstRefresh_clicked()
{
    QTableWidget *tbl = ui->tbl_modInstalled;
    QDir *dir = AppConfig::getModFolder();

    dir->setFilter(QDir::AllDirs);

    QStringList foldernames = dir->entryList();

    tbl->setEditTriggers(QAbstractItemView::NoEditTriggers);
    tbl->setRowCount(0);

    for (int i=2; i<foldernames.length(); i++) {
        tbl->insertRow(i-2);
        tbl->setItem(i-2,0, new QTableWidgetItem(foldernames.at(i)));
    }
}

void MainWindow::on_but_modDelete_clicked()
{
    QTableWidget *tbl = ui->tbl_modInstalled;

    // Remove from config table
    QTableWidget *cfg = ui->tbl_cfgLines;
    QStack<int> rows_to_delete;
    QList<QTableWidgetItem*> list = cfg->findItems(tbl->currentItem()->text(), Qt::MatchContains);

    for (QTableWidgetItem* item: list)
        rows_to_delete.push(item->row());

    while (rows_to_delete.length() > 0)
        ui->tbl_cfgLines->removeRow(rows_to_delete.pop());

    // Save to config file
    MainWindow::on_but_cfgSave_clicked();
    MainWindow::on_but_cfgReload_clicked();

    // Remove folder from disk
    QDir dir(AppConfig::getModFolderPath() + "/" + tbl->currentItem()->text());
    dir.removeRecursively();

    // Remove from mod table
    tbl->removeRow(tbl->currentRow());
}
