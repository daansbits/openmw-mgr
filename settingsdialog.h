#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QDialog>

namespace Ui {
class SettingsDialog;
}

class SettingsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SettingsDialog(QWidget *parent = 0);
    ~SettingsDialog();

private slots:
    void on_txt_pathCfgFile_textChanged(const QString &arg1);

    void on_txt_pathArchFolder_textChanged(const QString &arg1);

    void on_txt_pathModFolder_textChanged(const QString &arg1);

    void on_but_browseCfgFile_clicked();

    void on_but_browseArchFolder_clicked();

    void on_but_browseModFolder_clicked();

    void on_ok_cancel_clicked();

    void on_but_ok_clicked();

    void on_but_detectUnar_clicked();

private:
    Ui::SettingsDialog *ui;
    bool checkFormStatus();
};

#endif // SETTINGSDIALOG_H
