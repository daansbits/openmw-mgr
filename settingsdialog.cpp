#include "appconfig.h"
#include "settingsdialog.h"
#include "ui_settingsdialog.h"

#include <QDir>
#include <QFileDialog>
#include <QDebug>

SettingsDialog::SettingsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SettingsDialog)
{
    ui->setupUi(this);

    ui->txt_pathArchFolder->setText(AppConfig::getArchFolder()->absolutePath());
    ui->txt_pathCfgFile->setText(AppConfig::getOpenmwCfgFilePath());
    ui->txt_pathModFolder->setText(AppConfig::getModFolderPath());
    ui->txt_pathUnar->setText(AppConfig::getUnarExecPath());

    ui->but_ok->setEnabled(checkFormStatus());
}

SettingsDialog::~SettingsDialog()
{
    delete ui;
}

bool SettingsDialog::checkFormStatus() {
    QString archs = ui->txt_pathArchFolder->text();
    QString mods = ui->txt_pathModFolder->text();
    QString cfg = ui->txt_pathCfgFile->text();
    QString unar = ui->txt_pathUnar->text();

    bool all_selections_exist = QDir(archs).exists() && QDir(mods).exists() && QFile(cfg).exists() && QFile(unar).exists();
    bool no_empty_fields = archs.length() > 0 && mods.length() > 0 && cfg.length() > 0 && unar.length();

    return (no_empty_fields && all_selections_exist);
}

void SettingsDialog::on_txt_pathCfgFile_textChanged(const QString &)
{
    ui->but_ok->setEnabled(checkFormStatus());
}

void SettingsDialog::on_txt_pathArchFolder_textChanged(const QString &)
{
    ui->but_ok->setEnabled(checkFormStatus());
}

void SettingsDialog::on_txt_pathModFolder_textChanged(const QString &)
{
    ui->but_ok->setEnabled(checkFormStatus());
}

void SettingsDialog::on_but_browseCfgFile_clicked()
{
    QString os = QSysInfo::kernelType();
    QString confFileFolder;

    if (os == "linux") {
        confFileFolder = QDir::homePath() + "/.config/openmw";
    }
    else {
        confFileFolder = QDir::homePath();
    }
    QFileDialog confFileDialog(this);
    confFileDialog.setFileMode(QFileDialog::ExistingFile);
    confFileDialog.setNameFilter("OpenMW config file (openmw.cfg)");
    confFileDialog.setViewMode(QFileDialog::List);
    confFileDialog.setDirectory(confFileFolder);

    if (confFileDialog.exec()) {
        ui->txt_pathCfgFile->setText(confFileDialog.selectedFiles().first());
    }
}

void SettingsDialog::on_but_browseArchFolder_clicked()
{
    QFileDialog confFileDialog(this);
    confFileDialog.setFileMode(QFileDialog::Directory);
    confFileDialog.setOption(QFileDialog::ShowDirsOnly, true);
    confFileDialog.setNameFilter("Folder");
    confFileDialog.setViewMode(QFileDialog::List);

    if (confFileDialog.exec()) {
        ui->txt_pathArchFolder->setText(confFileDialog.selectedFiles().first());
    }
}

void SettingsDialog::on_but_browseModFolder_clicked()
{
    QFileDialog confFileDialog(this);
    confFileDialog.setFileMode(QFileDialog::Directory);
    confFileDialog.setOption(QFileDialog::ShowDirsOnly, true);
    confFileDialog.setNameFilter("Folder");
    confFileDialog.setViewMode(QFileDialog::List);

    if (confFileDialog.exec()) {
        ui->txt_pathModFolder->setText(confFileDialog.selectedFiles().first());
    }
}

void SettingsDialog::on_ok_cancel_clicked()
{
    return this->done(QDialog::Rejected);
}

void SettingsDialog::on_but_ok_clicked()
{
    AppConfig::setModFolder(ui->txt_pathModFolder->text());
    AppConfig::setArchFolder(ui->txt_pathArchFolder->text());
    AppConfig::setOpenmwCfgFilePath(ui->txt_pathCfgFile->text());
    AppConfig::setUnarExecPath(ui->txt_pathUnar->text());
    AppConfig::writeToSettingsFile();
    return this->done(QDialog::Accepted);
}

void SettingsDialog::on_but_detectUnar_clicked()
{
    ui->txt_pathUnar->setText(AppConfig::detectUnarExecPath());
    ui->but_ok->setEnabled(checkFormStatus());
}
