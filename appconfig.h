#ifndef CONFIG_H
#define CONFIG_H

#include <QDir>
#include <QFile>
#include <QString>


class AppConfig
{
public:
    static QFile* getOpenmwCfgFile();
    static QString getOpenmwCfgFilePath();
    static void setOpenmwCfgFilePath(const QString &value);

    static QDir* getArchFolder();
    static QString getArchFolderPath();
    static void setArchFolder(const QString &value);

    static QDir* getModFolder();
    static QString getModFolderPath();
    static void setModFolder(const QString &value);

    static QString getUnarExecPath();
    static QString detectUnarExecPath();
    static void setUnarExecPath(const QString &value);

    static void writeToSettingsFile();
    static void readFromSettingsFile();
    static int checkSettingsFileHealth();

private:
    AppConfig();
    static QString openmwCfgFilePath;
    static QString archFolderPath;
    static QString modFolderPath;
    static QString unarExecPath;
    static QFile openmwCfgFile;
    static QDir archFolder;
    static QDir modFolder;
    static void createSettingsFile();
};

enum SettingsHealth {
    SUCCESS = 0,
    FILE_NOT_FOUND = 1,
    COULD_NOT_OPEN = 2,
    BROKEN_PAIR = 3,
    MISSING_SETTINGS = 4
};

#endif // CONFIG_H
