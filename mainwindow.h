#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void extractArchive(QString archivePath, QString targetParentFolder);
    void refreshAllTabs();
private slots:
    void on_but_cfgReload_clicked();

    void on_but_cfgSave_clicked();

    void on_but_modArchRefresh_clicked();

    void on_tbl_modArch_itemSelectionChanged();

    void on_tbl_modInstalled_itemSelectionChanged();

    void on_but_settings_clicked();

    void on_but_modExtract_clicked();

    void on_but_modInstRefresh_clicked();

    void on_but_modDelete_clicked();

private:
    Ui::MainWindow *ui;
    void addModToCfg(QString path);
};

#endif // MAINWINDOW_H
