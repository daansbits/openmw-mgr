#include "appconfig.h"

#include <QDebug>
#include <QProcess>

QString AppConfig::openmwCfgFilePath = "";
QString AppConfig::archFolderPath = "";
QString AppConfig::modFolderPath = "";
QString AppConfig::unarExecPath = AppConfig::detectUnarExecPath();

QFile AppConfig::openmwCfgFile;
QDir AppConfig::archFolder;
QDir AppConfig::modFolder;

const int AMOUNT_OF_CONFIG_LINES = 4;

AppConfig::AppConfig()
{

}

QString AppConfig::detectUnarExecPath()
{
    QString os = QSysInfo::kernelType();

    if (os == "linux") {
        QProcess prog;
        prog.start("which unar");
        prog.waitForFinished();
        return QString(prog.readAllStandardOutput()).remove('\n');
    }
    return "";
}

void AppConfig::setUnarExecPath(const QString &value)
{
    unarExecPath = value;
}

QString AppConfig::getUnarExecPath()
{
    return unarExecPath;
}

QDir* AppConfig::getModFolder()
{
    return &modFolder;
}

QString AppConfig::getModFolderPath() {
    return modFolderPath;
}

void AppConfig::setModFolder(const QString &value)
{
    modFolderPath = value;
    modFolder.setPath(modFolderPath);
}

QDir* AppConfig::getArchFolder()
{
    return &archFolder;
}

QString AppConfig::getArchFolderPath() {
    return archFolderPath;
}

void AppConfig::setArchFolder(const QString &value)
{
    archFolderPath = value;
    archFolder.setPath(archFolderPath);
}

QFile* AppConfig::getOpenmwCfgFile()
{
    return &openmwCfgFile;
}

QString AppConfig::getOpenmwCfgFilePath()
{
    return openmwCfgFilePath;
}

void AppConfig::setOpenmwCfgFilePath(const QString &value)
{
    openmwCfgFilePath = value;
    openmwCfgFile.setFileName(openmwCfgFilePath);
}

int AppConfig::checkSettingsFileHealth() {
    QFile file("settings.cfg");

    if (!file.exists()) {
        return SettingsHealth::FILE_NOT_FOUND;
    }

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return SettingsHealth::COULD_NOT_OPEN;

    int lineCount = 0;
    while (!file.atEnd()) {
        QString line = QString::fromStdString(file.readLine().toStdString());
        if (line.length() > 0) {
            QList<QString> keyval = line.split('=');

            if (keyval.length() != 2) {
                file.close();
                return SettingsHealth::BROKEN_PAIR;
            }
            lineCount++;
        }
    }
    if (lineCount != AMOUNT_OF_CONFIG_LINES) {
        file.close();
        return SettingsHealth::MISSING_SETTINGS;
    }

    file.close();
    return SettingsHealth::SUCCESS;
}

void AppConfig::readFromSettingsFile() {
    QFile file("settings.cfg");
    file.open(QIODevice::ReadOnly | QIODevice::Text);

    while(!file.atEnd()) {
        QString line = QString::fromStdString(file.readLine().toStdString()).remove('\n');
        if (line.length() > 0) {
            QList<QString> keyval = line.split('=');

            if (keyval[0] == "openmw_config")
                setOpenmwCfgFilePath(keyval[1]);
            else if (keyval[0] == "openmw_modarchivefolder")
                setArchFolder(keyval[1]);
            else if (keyval[0] == "openmw_modinstallfolder")
                setModFolder(keyval[1]);
            else if (keyval[0] == "unar_path")
                setUnarExecPath(keyval[1]);
            else {
                qWarning() << "Unknown value discovered in config file: " + keyval[1];
            }
        }
    }
    file.close();
}

void AppConfig::writeToSettingsFile()
{
    QList<QString> lines;
    lines.append("openmw_config="+openmwCfgFilePath);
    lines.append("openmw_modarchivefolder="+archFolderPath);
    lines.append("openmw_modinstallfolder="+modFolderPath);
    lines.append("unar_path="+unarExecPath);

    QFile file("settings.cfg");
    file.open(QIODevice::ReadWrite | QIODevice::Text);
    QTextStream out(&file);

    for (QString line : lines) {
        qWarning() << "Writing: " + line;
        out << line << "\n";
    }
    file.flush();
    file.close();
}

void AppConfig::createSettingsFile() {
    QFile file("settings.cfg");
    file.open(QIODevice::WriteOnly | QIODevice::Text);
    file.flush();
    file.close();
}

