#include "mainwindow.h"
#include "appconfig.h"
#include "settingsdialog.h"
#include <QApplication>
#include <QDebug>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    int settingsHealth = AppConfig::checkSettingsFileHealth();

    if (settingsHealth != SettingsHealth::SUCCESS) {
        SettingsDialog set(&w);
        if (!set.exec())
            return 0;
    }
    else {
        AppConfig::readFromSettingsFile();
    }
    w.refreshAllTabs();
    return a.exec();
}
